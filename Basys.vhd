----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:56:04 03/07/2019 
-- Design Name: 
-- Module Name:    Basys - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package Basys2 is
	subtype LcdDigit is std_logic_vector(7 downto 0);
	type LcdDigits is array(3 downto 0) of LcdDigit;
end Basys2;

package body Basys2 is

end Basys2;

