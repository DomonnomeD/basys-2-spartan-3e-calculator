----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    19:29:26 09/15/2012 
-- Design Name: 
-- Module Name:    Basys2Lcd - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------

-- 7	1: LCD on 	<--> 0: LCD off
-- 6	1: Dot on 	<--> 0: Dot off
-- 5	1: Blink on <--> 0: Blink off
-- 4	1: Minus on <--> 0: Minus off		<- if on, hex value not show
-- 3..0	Hex value

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;
use work.Basys2.ALL;

entity Basys2Lcd is   
	Port (  
		  -- taktinis signalas
		  clk :  in  std_logic;
		  -- kokias busenas rodyti kiekvienam skaitmeniui
		  digits : in LcdDigits;
		  seg : out   std_logic_vector (6 downto 0); -- simboliu katodu aktyvavimo signalai 
        an  : out   std_logic_vector (3 downto 0); -- anodu aktyvavimo signalai
		  dp  : out   std_logic                      -- tasko katodo aktyvavimo signalas
	  );		  
end Basys2Lcd;

architecture Behavioral of Basys2Lcd is
   constant COUNTER_SIZE : integer := 20;--5;
	signal clkCnt : unsigned (COUNTER_SIZE-1 downto 0) := (others => '0'); 
	alias activeDigit : unsigned (1 downto 0) is clkCnt(COUNTER_SIZE-1 downto COUNTER_SIZE-2);
	--signal activeSeg : unsigned (1 downto 0);  -- aktyvaus segmento numeris
	signal anodes: std_logic_vector (3 downto 0);
	
		--mine
	constant COUNTER_SIZE_my : integer := 25;
	signal clkCnt_my : unsigned (COUNTER_SIZE_my-1 downto 0) := (others => '0'); 
	alias clkCnt_my_lastbit : std_logic is clkCnt_my(COUNTER_SIZE_my-1);	
	
begin

  clkDivider: process(clk)
  begin
    if clk'event and clk='1' then
      clkCnt <= clkCnt + 1;		
    end if;
  end process;  
  
  my_delay: process(clk)
  begin
    if clk'event and clk='1' then
      clkCnt_my <= clkCnt_my + 1;		
    end if;
  end process; 
  
  an <= anodes;
  
  segmentActivator: process(activeDigit, digits)
  begin   	 
--      0
--     ---  
--  5 |   | 1
--     ---   <- 6
--  4 |   | 2
--     ---
--      3	 
	
	if digits(to_integer(activeDigit))(4) = '1' then
		seg <= "0111111";  -- minus
	else
		case digits(to_integer(activeDigit))(3 downto 0) is
			when "0000" => seg <= "1000000";  --0
			when "0001" => seg <= "1111001";  --1
			when "0010" => seg <= "0100100";  --2
			when "0011" => seg <= "0110000";  --3
			when "0100" => seg <= "0011001";  --4
			when "0101" => seg <= "0010010";  --5
			when "0110" => seg <= "0000010";  --6
			when "0111" => seg <= "1111000";  --7
			when "1000" => seg <= "0000000";  --8
			when "1001" => seg <= "0010000";  --9
			when "1010" => seg <= "0001000";  --A
			when "1011" => seg <= "0000011";  --b
			when "1100" => seg <= "1000110";  --C
			when "1101" => seg <= "0100001";  --d
			when "1110" => seg <= "0000110";  --E
			when others => seg <= "0001110";  --F		 
		end case;
	end if;

  end process;  
  
  digitActivator: process(activeDigit, digits)
  begin	 
	 
	 for i in 3 downto 0 loop
		 if digits(i)(5) = '1' then
				if i = to_integer(activeDigit) and clkCnt_my_lastbit = '0' then
				  -- ijungiam skaiciaus rodyma, jei jis ijungtas skaiciaus konfiguracijos registre
				  anodes(i) <= '0' nor digits(to_integer(activeDigit))(7);
				else
				  -- isjungiam skaiciaus rodyma
				  anodes(i) <= '1';
				end if;
		else
				if i = to_integer(activeDigit) then
				  -- ijungiam skaiciaus rodyma, jei jis ijungtas skaiciaus konfiguracijos registre
				  anodes(i) <= '0' nor digits(to_integer(activeDigit))(7);
				else
				  -- isjungiam skaiciaus rodyma
				  anodes(i) <= '1';
				end if;
		end if;
	 
	 
	 
	 end loop;
 
  end process; 
  
  dotActivator: process(activeDigit, digits)
  begin	 
    -- jei ijungtas skaiciaus tasko rodymas, tai ijungiam tasko signala
	 dp <= not digits(to_integer(activeDigit))(6);
  end process; 	 

end Behavioral;

