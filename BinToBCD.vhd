library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;
use work.Basys2.ALL;


entity BinToBCD is
	port (
		-- sesioliktainis
		bin : in std_logic_vector(11 downto 0);
		-- desimtainis 3x4
		bcd : out std_logic_vector(11 downto 0)
	);
	end BinToBCD;
	
	
architecture arch of BinToBCD is

	begin

	converter : process ( bin )
		variable i : integer;
		-- 8 + 12 bit
		variable t : unsigned(23 downto 0);

	begin
		i := 0;
		
		t := (others => '0');
		t(11 downto 0) := unsigned(bin);
		
		for i in 0 to 11 loop
		
		-- postumis <-
		t := t(22 downto 0) & '0';
		
		--po 8 postumio nereikia tikrinti >4
		if(i < 11) then
			-- pridedam 3 jei daugiau nei 4
			if (t(15 downto 12) > "0100") then
				t(15 downto 12) := t(15 downto 12) + "0011";
			end if;
			
			if (t(19 downto 16) > "0100") then
				t(19 downto 16) := t(19 downto 16) + "0011";
			end if;		
		
			if (t(23 downto 20) > "0100") then
				t(23 downto 20) := t(23 downto 20) + "0011";
			end if;
		end if;
	end loop;
	
	bcd <= std_logic_vector(t(23 downto 12));
	end process;

end arch;

			












-- library IEEE;
-- use IEEE.STD_LOGIC_1164.ALL;
-- use IEEE.numeric_std.ALL;
-- use work.Basys2.ALL;


-- entity BinToBCD is
	-- port (
		---sesioliktainis
		-- bin : in std_logic_vector(7 downto 0);
		---desimtainis 3x4
		-- bcd : out std_logic_vector(11 downto 0)
	-- );
	-- end BinToBCD;
	
	
-- architecture arch of BinToBCD is

	-- begin

	-- converter : process ( bin )
		-- variable i : integer;
		---8 + 12 bit
		-- variable t : unsigned(19 downto 0);

	-- begin
		-- i := 0;
		
		-- t := (others => '0');
		-- t(7 downto 0) := unsigned(bin);
		
		-- for i in 0 to 7 loop
		
		----postumis <-
		-- t := t(18 downto 0) & '0';
		
		----po 8 postumio nereikia tikrinti >4
		-- if(i < 7) then
			----pridedam 3 jei daugiau nei 4
			-- if (t(11 downto 8) > "0100") then
				-- t(11 downto 8) := t(11 downto 8) + "0011";
			-- end if;
			
			-- if (t(15 downto 12) > "0100") then
				-- t(15 downto 12) := t(15 downto 12) + "0011";
			-- end if;		
		
			-- if (t(19 downto 16) > "0100") then
				-- t(19 downto 16) := t(19 downto 16) + "0011";
			-- end if;
		-- end if;
	-- end loop;
	
	-- bcd <= std_logic_vector(t(19 downto 8));
	-- end process;

-- end arch;

			