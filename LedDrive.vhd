library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.Basys2.ALL;

entity LedDrive is
	port(
		I_nputData	:	in	std_logic_vector(7 downto 0);
		O_utputData	:	out	std_logic_vector(7 downto 0)
	);
end LedDrive;

architecture arch of LedDrive is
begin

	-- O_utputData <= "11111111" when I_nputData="11110000" else
					-- "00000000";
	O_utputData <= I_nputData;
	--O_utputData <= I_nputData;
end arch;