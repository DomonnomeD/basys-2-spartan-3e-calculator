-- v_ variable
-- i_ input port
-- o_ output port
-- s_ signal

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.Basys2.ALL;

entity calc is
	Port (
		-- clk
		i_clk			: in std_logic;
		-- PS/2
		i_ps2_code_new 	: in std_logic;
		i_ps2_code     	: in std_logic_vector(7 DOWNTO 0);
		-- LCD
		o_digit			: out LcdDigits;
		-- led
		o_led			: out std_logic_vector(7 downto 0)
	);
end calc;
--------------------------------------------------------------------------------------------------------------------

architecture arch of calc is

	component BinToBCD is
		port (
			-- sesioliktainis
			bin : in std_logic_vector(11 downto 0);
			-- desimtainis 3x4
			bcd : out std_logic_vector(11 downto 0)
		);
	end component;

	-- signal reset : std_logic := '0';

	-- buffer values, max 4 with 8 vit each
	constant s_buf_size : integer := 8*3-1;
	signal s_1_buf : std_logic_vector(s_buf_size downto 0) := (others => '0');
	signal s_2_buf : std_logic_vector(s_buf_size downto 0) := (others => '0');
	signal lcd_buf : std_logic_vector(15 downto 0) := (others => '0');
	--button
	signal ReleaseButton : std_logic := '0';
	signal PressedButton : std_logic := '0';
	--budem ispolzovat kak bufer dlia dvuh znacenij
	--signal TmpBuf : std_logic_vector(15 downto 0) := (others => '0');
	
	signal s_buf1poscounter : unsigned(2 downto 0) := (others => '0');
	signal s_buf2poscounter : unsigned(2 downto 0) := (others => '0');
	
	
	-- for calc
	constant buf_size : integer := 4*3-1;
	signal s_calc_buf_1 : std_logic_vector(buf_size downto 0) := (others => '0');
			-- [1 cislo][2 ..][3 ..] v poriadke 3 downto 0
			--:= ("010100000000"); --500
			--      |   |   |
	signal s_calc_buf_2 : std_logic_vector(buf_size downto 0) := (others => '0');
			--:= ("010010011001"); --499
			--      |   |   |
	signal s_calc_1_sign : std_logic := '0';								--sign first input num
	signal s_calc_2_sign : std_logic := '0';								--sign second input num
	signal s_calc_operation : unsigned(1 downto 0) := "00";					--calc action:
																			-- 00 (0) -> plius
																			-- 01 (1) -> minus
																			-- 10 (2) -> mul
																			-- 11 (3) -> div
	signal s_calc_answer : std_logic_vector(buf_size downto 0);				--answer
	signal s_calc_sign : std_logic := '1';									--answer sign
	signal s_calc_num	: std_logic_vector(3 downto 0) := (others => '0');	--for knowing, how many num in answer
																			-- 1 <- "9"; 2 <- "99"; 3 <- "999"


	signal mode : std_logic_vector(1 downto 0) := "00";						-- 00 -> insert first value
																			-- 01 -> insert second value
																			-- 10 -> otvet


	-- convert bit to bcd
	signal bin : std_logic_vector(11 downto 0);
	signal answer_to_LCD : std_logic_vector(11 downto 0);
--------------------------------------------------------------------------------------------------------------------
begin
--------------------------------------------------------------------------------------------------------------------
	BinToBCDInst : BinToBCD
		port map (
			bin => bin,
			bcd => answer_to_LCD
		);
	
--------------------------------------------------------------------------------------------------------------------
	LedDriver : process (i_clk)
		variable tmp_led : std_logic_vector(7 downto 0);
	begin
		if i_clk'event and i_clk='1' then
		
			tmp_led := "00000000";
		
			if mode="00" then
				tmp_led := "00000001";
			elsif mode="01" then
				tmp_led := "00000010";
			elsif mode="10" then
				tmp_led := "00000011";
			end if;
			
			tmp_led(4 downto 2) := std_logic_vector(s_buf2poscounter(2 downto 0));
			tmp_led(7 downto 5) := std_logic_vector(s_buf1poscounter(2 downto 0));
			
			o_led <= tmp_led;
			
			-- for codes debug
			o_led <= i_ps2_code;
		end if;
	end process;
	
	
--------------------------------------------------------------------------------------------------------------------
	Calculator : process (i_clk)
		variable tmp_3 : integer := 0;
		variable tmp_2 : integer := 0;
		variable tmp_1 : integer := 0;
		variable tmp_1_mixed : integer := 0;
		variable tmp_2_mixed : integer := 0;
		variable tmp_answer : integer := 0;
		variable tmp_back_mixed : std_logic_vector(buf_size downto 0);
		
		-- for divider
		variable a1 : unsigned(12-1 downto 0):= (others => '0');
		variable b1 : unsigned(12-1 downto 0):= (others => '0');
		variable p1 : unsigned(12 downto 0):= (others => '0');

	begin
	
	if i_clk'event and i_clk='1' then
		-- mixing
		tmp_1 := to_integer(unsigned(s_calc_buf_1(3 downto 0)));
		tmp_2 := to_integer(unsigned(s_calc_buf_1(7 downto 4)));
		tmp_3 := to_integer(unsigned(s_calc_buf_1(11 downto 8)));
		-- tmp_3 := to_integer(unsigned(s_calc_buf_1(3 downto 0)));
		-- tmp_2 := to_integer(unsigned(s_calc_buf_1(7 downto 4)));
		-- tmp_1 := to_integer(unsigned(s_calc_buf_1(11 downto 8)));
		
		tmp_1_mixed := 0;
		
		if s_buf1poscounter=1 then
			tmp_1_mixed := tmp_1;
		elsif s_buf1poscounter=2 then
			tmp_1_mixed := tmp_2 + tmp_1 * 10;
		elsif s_buf1poscounter=3 or s_buf1poscounter=4 then
			tmp_1_mixed := tmp_3 + tmp_2 * 10 + tmp_1 * 100;
		end if;
		
		tmp_1 := to_integer(unsigned(s_calc_buf_2(3 downto 0)));
		tmp_2 := to_integer(unsigned(s_calc_buf_2(7 downto 4)));
		tmp_3 := to_integer(unsigned(s_calc_buf_2(11 downto 8)));
		-- tmp_3 := to_integer(unsigned(s_calc_buf_2(3 downto 0)));
		-- tmp_2 := to_integer(unsigned(s_calc_buf_2(7 downto 4)));
		-- tmp_1 := to_integer(unsigned(s_calc_buf_2(11 downto 8)));
		--tmp_2_mixed := tmp_3 * 100 + tmp_2 * 10 + tmp_1;
		tmp_2_mixed := 0;
		if s_buf2poscounter=1 then
			tmp_2_mixed := tmp_1;
		elsif s_buf2poscounter=2 then
			tmp_2_mixed := tmp_2 + tmp_1 * 10;
		elsif s_buf2poscounter=3 or s_buf2poscounter=4 then
			tmp_2_mixed := tmp_3 + tmp_2 * 10 + tmp_1 * 100;
		end if;
		
		--mixed to sign
		if s_calc_2_sign='1' then
			tmp_2_mixed := tmp_2_mixed * (-1);
		end if;	
		
		--mixed to sign
		if s_calc_1_sign='1' then
			tmp_1_mixed := tmp_1_mixed * (-1);
		end if;
		
		-- +
		if s_calc_operation="00" then
			tmp_answer := tmp_1_mixed + tmp_2_mixed;
			
			if tmp_answer<0 then
				s_calc_sign <= '1';
				-- need for good convert to usingned , becouse if "-X", unsigned be "Shit"
				tmp_answer := tmp_answer * (-1);
				tmp_back_mixed := std_logic_vector(to_unsigned(tmp_answer,buf_size+1));
			else
				s_calc_sign <= '0';
				tmp_back_mixed := std_logic_vector(to_unsigned(tmp_answer,buf_size+1));
			end if;

		-- -
		elsif s_calc_operation="01" then
			tmp_answer := tmp_1_mixed - tmp_2_mixed;
			
			if tmp_answer<0 then
				s_calc_sign <= '1';
				-- need for good convert to usingned , becouse if "-X", unsigned be "Shit"
				tmp_answer := tmp_answer * (-1);
				tmp_back_mixed := std_logic_vector(to_unsigned(tmp_answer,buf_size+1));
			else
				s_calc_sign <= '0';
				tmp_back_mixed := std_logic_vector(to_unsigned(tmp_answer,buf_size+1));
			end if;
			
		-- *				
		elsif s_calc_operation="10" then			
			--back to unsign
			if s_calc_2_sign='1' then
				tmp_2_mixed := tmp_2_mixed * (-1);
			end if;	
			if s_calc_1_sign='1' then
				tmp_1_mixed := tmp_1_mixed * (-1);
			end if;	
			
			tmp_answer := tmp_1_mixed * tmp_2_mixed;
			
			-- if tmp_answer<0 then
				-- s_calc_sign <= '1';
				-- tmp_back_mixed := std_logic_vector(to_unsigned(tmp_answer,buf_size+1));
			-- else
				-- s_calc_sign <= '0';
				-- tmp_back_mixed := std_logic_vector(to_unsigned(tmp_answer,buf_size+1));
			-- end if;
			
			if s_calc_2_sign='1' and s_calc_1_sign='1' then
				s_calc_sign <= '0';
			elsif (s_calc_2_sign='1' and s_calc_1_sign='0') or
			(s_calc_2_sign='0' and s_calc_1_sign='1') then
				s_calc_sign <= '1';
			elsif s_calc_2_sign='0' and s_calc_1_sign='0' then
				s_calc_sign <= '0';
			end if;
			
			tmp_back_mixed := std_logic_vector(to_unsigned(tmp_answer,buf_size+1));
			
			
		-- /		
		elsif s_calc_operation="11" then
		
			--back to unsign
			if s_calc_2_sign='1' then
				tmp_2_mixed := tmp_2_mixed * (-1);
			end if;	
			if s_calc_1_sign='1' then
				tmp_1_mixed := tmp_1_mixed * (-1);
			end if;
		
			a1 := to_unsigned(tmp_1_mixed,12);
			b1 := to_unsigned(tmp_2_mixed,12);
			p1 := (others => '0');
		
			for i in 0 to 12-1 loop
				p1(12-1 downto 1) := p1(12-2 downto 0);
				p1(0) := a1(12-1);
				a1(12-1 downto 1) := a1(12-2 downto 0);
				p1 := p1-b1;
				if(p1(12-1) ='1') then
					a1(0) :='0';
					p1 := p1+b1;
				else
					a1(0) :='1';
				end if;
			end loop;
		
			-- div algoritm working only if tmp_1_mixed>=tmp_2_mixed, if not, wrong answer
			if tmp_1_mixed>=tmp_2_mixed then
				tmp_back_mixed := std_logic_vector(a1);
			else
				tmp_back_mixed := (others => '1');	--make more than 999, so s_calc_num will be set "00"
			end if;
			--not test signed or not, but if prevous calc value signed, need override to not signed
			--s_calc_sign <= '0';
			
			if s_calc_2_sign='1' and s_calc_1_sign='1' then
				s_calc_sign <= '0';
			elsif (s_calc_2_sign='1' and s_calc_1_sign='0') or
			(s_calc_2_sign='0' and s_calc_1_sign='1') then
				s_calc_sign <= '1';
			elsif s_calc_2_sign='0' and s_calc_1_sign='0' then
				s_calc_sign <= '0';
			end if;
			
		end if;


		-- checking number in answer
		if unsigned(tmp_back_mixed) < 10 then
			s_calc_num(1 downto 0) <= "01";
		elsif  unsigned(tmp_back_mixed) < 100 then
			s_calc_num(1 downto 0) <= "10";
		elsif  unsigned(tmp_back_mixed) < 1000 then	
			s_calc_num(1 downto 0) <= "11";
		else
			s_calc_num(1 downto 0) <= "00";
		end if;

		bin <= tmp_back_mixed(11 downto 0);
	end if;	
	end process;


--------------------------------------------------------------------------------------------------------------------
	WatchButton : process (i_clk)
		variable TmpBuf : std_logic_vector(23 downto 0) := (others => '0');
	begin
		if i_clk'event and i_clk='1' then
		
			TmpBuf := TmpBuf(15 downto 0) & i_ps2_code;
			
			if  TmpBuf(15 downto 8)="11110000"	--release btn code
				and TmpBuf(15 downto 8) /= TmpBuf(7 downto 0) --we have data
				and TmpBuf(23 downto 16) /="11100000" --ne strelki
				and TmpBuf(15 downto 8) /="11100000" --ne strelki
				and TmpBuf(7 downto 0) /="11100000" --ne strelki
				then
			
			
				---- vvodim pervoe cislo
				if mode="00" then
					-- only num
					if  TmpBuf(7 downto 0) = "01110000" or
						TmpBuf(7 downto 0) = "01101001" or
						TmpBuf(7 downto 0) = "01110010" or
						TmpBuf(7 downto 0) = "01111010" or
						TmpBuf(7 downto 0) = "01101011" or
						TmpBuf(7 downto 0) = "01110011" or
						TmpBuf(7 downto 0) = "01110100" or
						TmpBuf(7 downto 0) = "01101100" or
						TmpBuf(7 downto 0) = "01110101" or
						TmpBuf(7 downto 0) = "01111101" then
						ReleaseButton <= '1';
					----proverka na znaki
					---- /
					elsif TmpBuf(7 downto 0) = "01001010" then
						s_calc_operation <= "11";
						ReleaseButton <= '0';
						mode <= "01";
					---- *
					elsif TmpBuf(7 downto 0) = "01111100" then
						s_calc_operation <= "10";
						ReleaseButton <= '0';
						mode <= "01";
					---- -
					elsif TmpBuf(7 downto 0) = "01111011" then
						s_calc_operation <= "01";
						ReleaseButton <= '0';
						mode <= "01";
					---- +
					elsif TmpBuf(7 downto 0) = "01111001" then
						s_calc_operation <= "00";
						ReleaseButton <= '0';						
						mode <= "01";
					--- reset
					-- elsif TmpBuf(7 downto 0) = "01110001" then
						-- reset <= '1';
						-- ReleaseButton <= '1';
					--- "-"
					elsif TmpBuf(7 downto 0) = "01001110" then
						s_calc_1_sign <= not s_calc_1_sign;
					else
						ReleaseButton <= '0';
					end if;
				end if;
				
				-- vvodim 2 cislo
				if mode="01" then
					-- only num
					if TmpBuf(7 downto 0) = "01110000" or
						TmpBuf(7 downto 0) = "01101001" or
						TmpBuf(7 downto 0) = "01110010" or
						TmpBuf(7 downto 0) = "01111010" or
						TmpBuf(7 downto 0) = "01101011" or
						TmpBuf(7 downto 0) = "01110011" or
						TmpBuf(7 downto 0) = "01110100" or
						TmpBuf(7 downto 0) = "01101100" or
						TmpBuf(7 downto 0) = "01110101" or
						TmpBuf(7 downto 0) = "01111101" then
						ReleaseButton <= '1';
						
					----proverka na znaki
					---- enter
					elsif TmpBuf(7 downto 0) = "01011010" then
						ReleaseButton <= '0';
						mode <= "10";
					--- "-"
					elsif TmpBuf(7 downto 0) = "01001110" then
						s_calc_2_sign <= not s_calc_2_sign;
					--- reset
					-- elsif TmpBuf(7 downto 0) = "01110001" then
						-- reset <= '1';
						-- ReleaseButton <= '1';
					end if;
				end if;	
				
				
				-- zdem nazatija cifri stob s nacala delat
				if mode="10" then
					-- only num
					if TmpBuf(7 downto 0) = "01110000" or
						TmpBuf(7 downto 0) = "01101001" or
						TmpBuf(7 downto 0) = "01110010" or
						TmpBuf(7 downto 0) = "01111010" or
						TmpBuf(7 downto 0) = "01101011" or
						TmpBuf(7 downto 0) = "01110011" or
						TmpBuf(7 downto 0) = "01110100" or
						TmpBuf(7 downto 0) = "01101100" or
						TmpBuf(7 downto 0) = "01110101" or
						TmpBuf(7 downto 0) = "01111101" then
						ReleaseButton <= '1';
						mode <= "00";
					--- reset
					-- elsif TmpBuf(7 downto 0) = "01110001" then
						-- reset <= '1';
						-- ReleaseButton <= '1';
					else
						ReleaseButton <= '0';
					end if;
				end if;		
				
			else
				ReleaseButton <= '0';
				--reset <= '0';
			end if;
		end if;
	end process;

--------------------------------------------------------------------------------------------------------------------
	WriteIntoBuf : process (i_clk)
		variable tmp1_bufposcounter : integer := 0;
		variable tmp2_bufposcounter : integer := 0;
		variable tmp1_buf : std_logic_vector(s_buf_size downto 0) := (others => '0');
		variable tmp2_buf : std_logic_vector(s_buf_size downto 0) := (others => '0');
		variable watch_mode : std_logic_vector(3 downto 0) := (others => '0');
	begin
		if i_clk'event and i_clk='1' then
		
			 -- watch_mode := watch_mode(3 downto 2) & mode;
			 -- if watch_mode(3 downto 2) /= watch_mode(1 downto 0) then
				 -- if mode = "00" then
					 -- tmp1_bufposcounter := 0;
					 -- tmp2_bufposcounter := 0;
					 -- tmp1_buf := (others => '0');
					 -- tmp2_buf := (others => '0');
					 -- s_buf1poscounter <= (others => '0');
					 -- s_buf2poscounter <= (others => '0');
				 -- end if;
			 -- end if;

			if ReleaseButton='1' and i_ps2_code_new='1' then
				
				watch_mode := watch_mode(1 downto 0) & mode;
				if watch_mode(3 downto 2) /= watch_mode(1 downto 0) then
					if mode = "00" then
						tmp1_bufposcounter := 0;
						tmp2_bufposcounter := 0;
						tmp1_buf := (others => '0');
						tmp2_buf := (others => '0');
						s_buf1poscounter <= (others => '0');
						s_buf2poscounter <= (others => '0');
					end if;
				end if;
			
				if mode="00" then	--stobi neobnuliala kogda drugoj rezim, ibo vmesto 123 polucaju 1
					if tmp1_bufposcounter = 3 then
						tmp1_bufposcounter := 0;
						tmp1_buf := (others => '0');	
					end if;
				end if;

				if mode="01" then
					if tmp2_bufposcounter = 3 then
						tmp2_bufposcounter := 0;
						tmp2_buf := (others => '0');	
					end if;		
				end if;	
			
				if mode="00" then
					tmp1_buf(8*tmp1_bufposcounter+8-1 downto 8*tmp1_bufposcounter) := i_ps2_code;
					s_1_buf <= tmp1_buf;	
					tmp1_bufposcounter := tmp1_bufposcounter + 1;
				elsif mode="01" then
					tmp2_buf(8*tmp2_bufposcounter+8-1 downto 8*tmp2_bufposcounter) := i_ps2_code;
					s_2_buf <= tmp2_buf;
					tmp2_bufposcounter := tmp2_bufposcounter + 1;
				end if;

				s_buf1poscounter <= to_unsigned(tmp1_bufposcounter,3);
				s_buf2poscounter <= to_unsigned(tmp2_bufposcounter,3);
				
			end if;
		end if;	
		
		
	end process;
	
	
--------------------------------------------------------------------------------------------------------------------
	ConvertToLCD : process (i_clk)
	begin
	
		if i_clk'event and i_clk='1' then
		
			if mode="00" then
				for i in 0 to 2 loop
					if s_1_buf(8*i+8-1 downto 8*i) = "01110000" then
						lcd_buf(4*i+4-1 downto 4*i) <= "0000";
					elsif s_1_buf(8*i+8-1 downto 8*i) = "01101001" then
						lcd_buf(4*i+4-1 downto 4*i) <= "0001";
					elsif s_1_buf(8*i+8-1 downto 8*i) = "01110010" then
						lcd_buf(4*i+4-1 downto 4*i) <= "0010";
					elsif s_1_buf(8*i+8-1 downto 8*i) = "01111010" then
						lcd_buf(4*i+4-1 downto 4*i) <= "0011";
					elsif s_1_buf(8*i+8-1 downto 8*i) = "01101011" then
						lcd_buf(4*i+4-1 downto 4*i) <= "0100";
					elsif s_1_buf(8*i+8-1 downto 8*i) = "01110011" then
						lcd_buf(4*i+4-1 downto 4*i) <= "0101";
					elsif s_1_buf(8*i+8-1 downto 8*i) = "01110100" then
						lcd_buf(4*i+4-1 downto 4*i) <= "0110";
					elsif s_1_buf(8*i+8-1 downto 8*i) = "01101100" then
						lcd_buf(4*i+4-1 downto 4*i) <= "0111";
					elsif s_1_buf(8*i+8-1 downto 8*i) = "01110101" then
						lcd_buf(4*i+4-1 downto 4*i) <= "1000";
					elsif s_1_buf(8*i+8-1 downto 8*i) = "01111101" then
						lcd_buf(4*i+4-1 downto 4*i) <= "1001";
					end if;
				end loop;
			elsif mode="01" then
				for i in 0 to 2 loop
					if s_2_buf(8*i+8-1 downto 8*i) = "01110000" then
						lcd_buf(4*i+4-1 downto 4*i) <= "0000";
					elsif s_2_buf(8*i+8-1 downto 8*i) = "01101001" then
						lcd_buf(4*i+4-1 downto 4*i) <= "0001";
					elsif s_2_buf(8*i+8-1 downto 8*i) = "01110010" then
						lcd_buf(4*i+4-1 downto 4*i) <= "0010";
					elsif s_2_buf(8*i+8-1 downto 8*i) = "01111010" then
						lcd_buf(4*i+4-1 downto 4*i) <= "0011";
					elsif s_2_buf(8*i+8-1 downto 8*i) = "01101011" then
						lcd_buf(4*i+4-1 downto 4*i) <= "0100";
					elsif s_2_buf(8*i+8-1 downto 8*i) = "01110011" then
						lcd_buf(4*i+4-1 downto 4*i) <= "0101";
					elsif s_2_buf(8*i+8-1 downto 8*i) = "01110100" then
						lcd_buf(4*i+4-1 downto 4*i) <= "0110";
					elsif s_2_buf(8*i+8-1 downto 8*i) = "01101100" then
						lcd_buf(4*i+4-1 downto 4*i) <= "0111";
					elsif s_2_buf(8*i+8-1 downto 8*i) = "01110101" then
						lcd_buf(4*i+4-1 downto 4*i) <= "1000";
					elsif s_2_buf(8*i+8-1 downto 8*i) = "01111101" then
						lcd_buf(4*i+4-1 downto 4*i) <= "1001";
					end if;
				end loop;
			elsif mode="10" then
				-- lcd_buf(11 downto 0) <= answer_to_LCD(3 downto 0) &
										-- answer_to_LCD(7 downto 4) &
										-- answer_to_LCD(11 downto 8);
				lcd_buf(11 downto 0) <= answer_to_LCD;
			end if;
		end if;
	
	end process;

--------------------------------------------------------------------------------------------------------------------
	ConvertToCalc : process (i_clk)
	begin
	
		if i_clk'event and i_clk='1' then
		
			if mode="00" then
				for i in 0 to 2 loop
					if s_1_buf(8*i+8-1 downto 8*i) = "01110000" then
						s_calc_buf_1(4*i+4-1 downto 4*i) <= "0000";
					elsif s_1_buf(8*i+8-1 downto 8*i) = "01101001" then
						s_calc_buf_1(4*i+4-1 downto 4*i) <= "0001";
					elsif s_1_buf(8*i+8-1 downto 8*i) = "01110010" then
						s_calc_buf_1(4*i+4-1 downto 4*i) <= "0010";
					elsif s_1_buf(8*i+8-1 downto 8*i) = "01111010" then
						s_calc_buf_1(4*i+4-1 downto 4*i) <= "0011";
					elsif s_1_buf(8*i+8-1 downto 8*i) = "01101011" then
						s_calc_buf_1(4*i+4-1 downto 4*i) <= "0100";
					elsif s_1_buf(8*i+8-1 downto 8*i) = "01110011" then
						s_calc_buf_1(4*i+4-1 downto 4*i) <= "0101";
					elsif s_1_buf(8*i+8-1 downto 8*i) = "01110100" then
						s_calc_buf_1(4*i+4-1 downto 4*i) <= "0110";
					elsif s_1_buf(8*i+8-1 downto 8*i) = "01101100" then
						s_calc_buf_1(4*i+4-1 downto 4*i) <= "0111";
					elsif s_1_buf(8*i+8-1 downto 8*i) = "01110101" then
						s_calc_buf_1(4*i+4-1 downto 4*i) <= "1000";
					elsif s_1_buf(8*i+8-1 downto 8*i) = "01111101" then
						s_calc_buf_1(4*i+4-1 downto 4*i) <= "1001";
					end if;
				end loop;
			elsif mode="01" then
				for i in 0 to 2 loop
					if s_2_buf(8*i+8-1 downto 8*i) = "01110000" then
						s_calc_buf_2(4*i+4-1 downto 4*i) <= "0000";
					elsif s_2_buf(8*i+8-1 downto 8*i) = "01101001" then
						s_calc_buf_2(4*i+4-1 downto 4*i) <= "0001";
					elsif s_2_buf(8*i+8-1 downto 8*i) = "01110010" then
						s_calc_buf_2(4*i+4-1 downto 4*i) <= "0010";
					elsif s_2_buf(8*i+8-1 downto 8*i) = "01111010" then
						s_calc_buf_2(4*i+4-1 downto 4*i) <= "0011";
					elsif s_2_buf(8*i+8-1 downto 8*i) = "01101011" then
						s_calc_buf_2(4*i+4-1 downto 4*i) <= "0100";
					elsif s_2_buf(8*i+8-1 downto 8*i) = "01110011" then
						s_calc_buf_2(4*i+4-1 downto 4*i) <= "0101";
					elsif s_2_buf(8*i+8-1 downto 8*i) = "01110100" then
						s_calc_buf_2(4*i+4-1 downto 4*i) <= "0110";
					elsif s_2_buf(8*i+8-1 downto 8*i) = "01101100" then
						s_calc_buf_2(4*i+4-1 downto 4*i) <= "0111";
					elsif s_2_buf(8*i+8-1 downto 8*i) = "01110101" then
						s_calc_buf_2(4*i+4-1 downto 4*i) <= "1000";
					elsif s_2_buf(8*i+8-1 downto 8*i) = "01111101" then
						s_calc_buf_2(4*i+4-1 downto 4*i) <= "1001";
					end if;
				end loop;
			end if;
		end if;
	
	end process;

	
--------------------------------------------------------------------------------------------------------------------
	LCDprintf : process (i_clk)
	begin
		if i_clk'event and i_clk='1' then
			--nado ucitivat sto teper s_bufposcounter dve stuki raznie est

			if mode="00" then
				if 	to_integer(s_buf1poscounter) > 2 then
					o_digit(0) <= ("10" & '0' & '0' & lcd_buf(11 downto 8));
				else
					o_digit(0) <= "00000000";
				end if;
				
				if 	to_integer(s_buf1poscounter) > 1 then
					o_digit(1) <= ("10" & '0' & '0' & lcd_buf(7 downto 4));
				else
					o_digit(1) <= "00000000";
				end if;
				
				if 	to_integer(s_buf1poscounter) > 0 then
					o_digit(2) <= ("10" & '0' & '0' & lcd_buf(3 downto 0));
				else
					o_digit(2) <= "00000000";
				end if;
			
			elsif mode="01" then
				if 	to_integer(s_buf2poscounter) > 2 then
					o_digit(0) <= ("10" & '0' & '0' & lcd_buf(11 downto 8));
				else
					o_digit(0) <= "00000000";
				end if;
				
				if 	to_integer(s_buf2poscounter) > 1 then
					o_digit(1) <= ("10" & '0' & '0' & lcd_buf(7 downto 4));
				else
					o_digit(1) <= "00000000";
				end if;
				
				if 	to_integer(s_buf2poscounter) > 0 then
					o_digit(2) <= ("10" & '0' & '0' & lcd_buf(3 downto 0));
				else
					o_digit(2) <= "00000000";
				end if;
			
			elsif mode="10" then
			
				if s_calc_num(1 downto 0)="01" then
					o_digit(2) <= ("10" & '0' & '0' & lcd_buf(3 downto 0));
					o_digit(1) <= "00000000";
					o_digit(0) <= "00000000";
				elsif s_calc_num(1 downto 0)="10" then
					o_digit(2) <= ("10" & '0' & '0' & lcd_buf(7 downto 4));
					o_digit(1) <= ("10" & '0' & '0' & lcd_buf(3 downto 0));
					o_digit(0) <= "00000000";
				elsif s_calc_num(1 downto 0)="11" then
					o_digit(2) <= ("10" & '0' & '0' & lcd_buf(11 downto 8));	
					o_digit(1) <= ("10" & '0' & '0' & lcd_buf(7 downto 4));
					o_digit(0) <= ("10" & '0' & '0' & lcd_buf(3 downto 0));
				elsif s_calc_num(1 downto 0)="00" then
					o_digit(2) <= "00000000";
					o_digit(1) <= "00000000";
					o_digit(0) <= "00000000";
				end if;
		
				-- o_digit(0) <= ("10" & '0' & '0' & lcd_buf(11 downto 8));	
				-- o_digit(1) <= ("10" & '0' & '0' & lcd_buf(7 downto 4));
				-- o_digit(2) <= ("10" & '0' & '0' & lcd_buf(3 downto 0));
			end if;
			
			
			if mode="00" then
				if s_calc_1_sign='1' then -- esli s minusom, to vivodim minus
					o_digit(3) <= ("10" & '0' & s_calc_1_sign & "0000");
				else -- esli ze s pliusom, to vikliucaem led
					o_digit(3) <= ("00" & '0' & s_calc_1_sign & "0000");
				end if;
			elsif mode="01" then
				if s_calc_2_sign='1' then -- esli s minusom, to vivodim minus
					o_digit(3) <= ("10" & '0' & s_calc_2_sign & "0000");
				else -- esli ze s pliusom, to vikliucaem led
					o_digit(3) <= ("00" & '0' & s_calc_2_sign & "0000");	
				end if;				
			elsif mode="10" then
				if s_calc_sign='1' then -- esli s minusom, to vivodim minus
					o_digit(3) <= ("10" & '0' & s_calc_sign & "0000");
				else -- esli ze s pliusom, to vikliucaem led
					o_digit(3) <= ("00" & '0' & s_calc_sign & "0000");	
				end if;
			end if;
		
		end if;
	end process;
	
end arch;






