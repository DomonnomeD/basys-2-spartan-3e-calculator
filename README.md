### This code done by FPGA board Basys 2 spartan 3e.
- Input: by keyboard wth ps/2 interface (fpga can handle number and "+","-","*","/",",","=")
- Output: by 8 segment x4 screens (show number and dot)
- Working loop:
    + Input first number 
	+ Press one ot these: "+","-","*","/"
    + Input second number
    + Press "="
    + On screen wil be answer

#### Top entity: [Top Layer.pdf](Top Layer.pdf)

#### Program limits (not realized thing):
- When dividing, only the number before the comma is displayed for example, the number 12.34 will display 12
- instead of erasing inputted number, overflow is implemented to enter a number from the beginning