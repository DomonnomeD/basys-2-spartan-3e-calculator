----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:48:46 03/07/2019 
-- Design Name: 
-- Module Name:    Top - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;
use work.Basys2.ALL;

entity Top is
	port (
		mclk	:	in 	std_logic;
		btn		:	in	std_logic_vector (3 downto 0);
		-- LCD
		seg		:	out	std_logic_vector (6 downto 0);
		an		:	out	std_logic_vector (3 downto 0);
		dp		:	out	std_logic;
		-- dlia klavi
		PS2C	:	inout 	std_logic;
		PS2D	:	inout 	std_logic;
		-- Debug
		led		: 	out		std_logic_vector (7 downto 0)
	);
end Top;

architecture Behavioral of Top is

	component Basys2Lcd is
		Port (
			clk		:	in	std_logic;
			digits	:	in	LcdDigits;
			seg		:	out	std_logic_vector (6 downto 0);
			an		:	out	std_logic_vector (3 downto 0);
			dp		:	out	std_logic
		);
	end component;
	
	component ps2_keyboard is
		Port(
			clk          : in  std_logic;                     
			ps2_clk      : in  std_logic;                     
			ps2_data     : in  std_logic;                     
			ps2_code_new : out std_logic;                     
			ps2_code     : out std_logic_vector(7 DOWNTO 0) 
		);
	end component;	

	component calc is
	Port (
		i_clk			: in std_logic;
		i_ps2_code_new 	: in std_logic;
		i_ps2_code     	: in std_logic_vector(7 DOWNTO 0);
		o_digit			: out LcdDigits;
		o_led			: out std_logic_vector(7 downto 0)
	);
	end component;	
	
	component LedDrive is
		port(
			I_nputData	:	in	std_logic_vector(7 downto 0);
			O_utputData	:	out	std_logic_vector(7 downto 0)
		);
	end component;
	
	-- component ButonPressBtnDecoder is
		-- port(
			-- i_ps2_code_new : in std_logic;
			-- I_clk		:	in	std_logic;
			-- I_nputData	:	in	std_logic_vector(7 downto 0);
			-- O_utputData	:	out	std_logic_vector(7 downto 0)
		-- );
	-- end component;

	--klava
	signal ps2_code_new : std_logic;                     
	signal ps2_code     : std_logic_vector(7 DOWNTO 0); 
	-- ekran
	signal digits : LcdDigits;
	-- calc

	-- signal RealData			: std_logic_vector(7 DOWNTO 0);
	signal o_led			: std_logic_vector(7 DOWNTO 0);

begin

	Basys2LcdInst : Basys2Lcd
		port map (
			clk => mclk,
			digits => digits,
			seg => seg,
			an => an,
			dp => dp 
		);
			
	ps2_keyboardInst : ps2_keyboard
		port map (
			clk => mclk,            
			ps2_clk => PS2C,                 
			ps2_data => PS2D,                   
			ps2_code_new => ps2_code_new,                
			ps2_code => ps2_code
		);
		
	calcInst : calc
		port map(
			i_clk => mclk,
			i_ps2_code_new => ps2_code_new,
			i_ps2_code => ps2_code,
			o_digit => digits,
			o_led => o_led
		);
		
	LedDriveInst : LedDrive
		port map(
			I_nputData => o_led,
			O_utputData => led
		);	
		
	-- sss : ButonPressBtnDecoder
		-- port map(
			-- i_ps2_code_new => ps2_code_new,
			-- I_clk => mclk,
			-- I_nputData => ps2_code,
			-- O_utputData => RealData
		-- );
		

end Behavioral;

