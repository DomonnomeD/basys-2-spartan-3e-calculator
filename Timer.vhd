library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;
use work.Basys2.ALL;

entity timer is
	Port (
		--taktiniai signalai
		clk		:	in		std_logic;
		-- LCD ekrano valdymas
		digits	:	out		LcdDigits;
		-- mygtuku buenu nuskaitymas
		btn		:	in		std_logic_vector (1 downto 0) 
	);
end timer;

architecture timeris of timer is

	component BinToBCD is
		port (
			-- sesioliktainis
			bin : in std_logic_vector(7 downto 0);
			-- desimtainis 3x4
			bcd : out std_logic_vector(11 downto 0)
		);
	end component;



	constant 	CLK_COUNTER_SIZE	:	integer := 18;
	--constant 	CLK_COUNTER_MAX		:	unsigned(CLK_COUNTER_SIZE - 1 downto 0) := "000000000000001111";--:= "101111101011110000";
	constant 	CLK_COUNTER_MAX		:	unsigned(CLK_COUNTER_SIZE - 1 downto 0) := "101111101011110000";
	signal 		clk_counter			:	unsigned(CLK_COUNTER_SIZE - 1 downto 0) := (others => '0'); --uvelicivaetia do CLK_COUNTER_MAX
	
	constant 	TIMER_START			:	unsigned(12 downto 0) := "1110111111111";
	signal		timer_counter		:	unsigned(12 downto 0) := (others => '0');
	signal 		timer 				:	std_logic_vector(12 downto 0);
	signal 		timer_enable		:	std_logic := '0'; --1 esli razresaem scitat
	signal 		timer_clk			:	std_logic := '0'; --posle togo kak clk_counter = CLK_COUNTER_MAX v 1, po nemu vse ostalnoe rabotaet, od delitel skorosti
	alias		btn_reset			: 	std_logic is btn(0);
	alias		btn_stop			:	std_logic is btn(1);

	-- saugoma pries tai buvusi stop/start btn busena
	signal btn_stop_prev : std_logic;

	--sek skaitiklis, nuo 30 pradeda
	constant SEC_START : unsigned(7 downto 0) := "00011110";
	signal sec_counter : unsigned(7 downto 0) := (others => '0');
	--msec -//-, 00->99->98
	constant MSEC_START : unsigned(7 downto 0) := "00000000";
	constant MSEC_MAC : unsigned(7 downto 0) := "01100011";
	signal msec_counter : unsigned(7 downto 0) := (others => '0');
	
	signal msec_clk : std_logic := '0';
	signal sec_clk : std_logic := '0';
	
	-- desimtaines reiskmes
	signal decSec, decMsec : std_logic_vector (11 downto 0);
begin

	BinToBCD_Sec : BinToBCD
		port map (
			bin => std_logic_vector(sec_counter),
			bcd => decSec
		);
		
	BinToBCD_Msec : BinToBCD
		port map (
			bin => std_logic_vector(msec_counter),
			bcd => decMsec
		);	

	ClockDivider: process(clk, btn_reset)
	begin
		--paspaudus reset, laikas is pradziu
		if btn_reset='1' then
			clk_counter <= (others => '0');
			msec_clk <= '0';
		else
			if clk'event and clk='1' then
				if timer_enable='1' then
					--ziurim ar clk_counter pasieke max
					if clk_counter = CLK_COUNTER_MAX then
						clk_counter <= (others => '0');
						msec_clk <= '1';
					else
						msec_clk <= '0';
						clk_counter <= clk_counter + 1;
					end if;
				else
					msec_clk <= '0';
					clk_counter <= clk_counter;
				end if;
				btn_stop_prev <= btn_stop;
			end if;
		end if;
	end process;
	
	MsecTick : process (msec_clk, btn_reset)
	begin
		-- if btn reset, skaiciuojam is naujo
		if btn_reset='1' then
			msec_counter <= MSEC_START;
			sec_clk <= '0';
		else
			if msec_clk'event and msec_clk='1' then
				if msec_counter = MSEC_START then
					msec_counter <= msec_counter - 1;
					sec_clk <= '1';
				elsif msec_counter=0 then
					msec_counter <= MSEC_START;
					sec_clk <= '0';
				else
					msec_counter <= msec_counter - 1;
					sec_clk <= '0';
				end if;
			end if;
		end if;
	end process;
	
	SecTick : process(sec_clk, btn_reset)
	begin
		-- if btn reset, skaiciuojam is naujo
		if btn_reset='1' then
			sec_counter <= SEC_START;
		else
			if sec_clk'event and sec_clk='1' then
				if sec_counter=0 then
					sec_counter <= SEC_START;
				else
					sec_counter <= sec_counter - 1;
				end if;
			end if;
		end if;
	end process;
	
	TimerTick : process(timer_clk, btn_reset)
	begin
		--paspaudus reset mygtuka, is naujo laikas
		if btn_reset='1' then
			timer_counter <= TIMER_START;
		else
			if timer_clk'event and timer_clk='1' then
				timer_counter <= timer_counter - 1;
			end if;
		end if;
	end process;
	
	TimerStop : process(clk, btn_reset, btn_stop, timer_enable)
	begin
		-- asinchroninis timerio reset
		if btn_reset='1' then
			timer_enable <= '1';
		else
			if clk'event and clk='1' then
				if sec_counter=0 and msec_counter=0 then
					timer_enable <= '0';
				elsif btn_stop='1' and btn_stop /= btn_stop_prev then
					timer_enable <= not timer_enable;
				end if;
			end if;
		end if;
	end process;

	timer <= std_logic_vector(timer_counter);
	
	digits(3) <= ("10" & not timer_enable & '0' & decSec(7 downto 4));
	digits(2) <= ("10" & not timer_enable & '0' & decSec(3 downto 0));
	digits(1) <= ("10" & not timer_enable & '0' & decMsec(7 downto 4));
	digits(0) <= ("10" & not timer_enable & '0' & decMsec(3 downto 0));

end timeris;


