library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.Basys2.ALL;

entity ButonPressBtnDecoder is
	port(
		i_ps2_code_new 		: 	in std_logic;
		I_clk				:	in	std_logic;
		I_nputData			:	in	std_logic_vector(7 downto 0);
		O_utputData			:	out	std_logic_vector(7 downto 0)
	);
end ButonPressBtnDecoder;

architecture arch of ButonPressBtnDecoder is

	-- tolko dva znacenija sohraniaem
	signal Buf : std_logic_vector(8*2-1 downto 0);

begin

	DecodeWhenBtnPressed : process (I_clk)
		variable counter : integer := 0;
	begin
		
		if I_clk'event and I_clk='1' then
		
			if i_ps2_code_new = '1' then
		
				if counter = 0 then
					Buf(8*counter+8-1 downto 8*counter) <= Buf;
					counter := counter + 1;
				elsif counter = 1 then
					Buf(8*counter+8-1 downto 8*counter) <= Buf;
					counter := 0;
				end if;
				
				if Buf(8*0+8-1 downto 8*0) = Buf(8*1+8-1 downto 8*1) then
					O_utputData <= Buf(8*0+8-1 downto 8*0);
				end if;

			end if;
		end if;

	end process;

end arch;